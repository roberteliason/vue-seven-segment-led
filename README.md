# SevenSegmentDisplays
A tribute to the queen of old-timey displays: Fran Blanche!
https://www.youtube.com/Fran%20Blanche

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
